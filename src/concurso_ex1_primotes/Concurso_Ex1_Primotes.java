/*
package concurso_ex1_primotes;
*/

import java.util.Scanner;

/**
 *
 * @author PASSAGEIROS
 */
public class Concurso_Ex1_Primotes {

    public static void main(String[] args) {

        int intervalos[][] = entrada();
        int count;

        for (int i = 0; i < intervalos.length; i++) {
            count = numPrimotes(intervalos[i][0], intervalos[i][1]);
            if (count == 0) {
                System.out.format("Não há primotes.%n");
            } else
                if(count == 1) {
                    System.out.format("1 primote.%n");
                }else {
                    System.out.format("%d primotes.%n",count);
                }
        }

    }

    private static int[][] entrada() {
        Scanner scan = new Scanner(System.in);
        int intervalos[][] = new int[1][2];

        int numIntervalos = 0;
        System.out.format("Introduza os valores%n");
        do {

            if (intervalos.length == numIntervalos + 1) {
                intervalos = novoArray(intervalos, intervalos.length + 10, 2);
            }

            String str[] = scan.nextLine().split(" ");
            if (str.length != 2 && !str[0].equals("-1")) {
                System.out.format("Entrada inválida%n");
                numIntervalos--;
            } else {
                intervalos[numIntervalos][0] = Integer.parseInt(str[0]);
                if (intervalos[numIntervalos][0] != -1) {
                    intervalos[numIntervalos][1] = Integer.parseInt(str[1]);
                } else {
                    break;
                }

                if ((intervalos[numIntervalos][0] > 1000000 || intervalos[numIntervalos][0] < 100)
                        || (intervalos[numIntervalos][1] <= intervalos[numIntervalos][0] || intervalos[numIntervalos][1] > 1000000)) {
                    System.out.format("Entrada inválida%n");
                    numIntervalos--;
                }
            }

            numIntervalos++;
        } while (intervalos[numIntervalos][0] != -1);

        return novoArray(intervalos, numIntervalos, 2);
    }

    private static int[][] novoArray(int[][] array, int numLinhas, int numColunas) {

        int novo[][] = new int[numLinhas][numColunas];

        if (array.length < numLinhas) {
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[i].length; j++) {
                    novo[i][j] = array[i][j];
                }
            }
        } else {
            for (int i = 0; i < novo.length; i++) {
                for (int j = 0; j < novo[i].length; j++) {
                    novo[i][j] = array[i][j];
                }
            }
        }

        return novo;
    }

    private static boolean primo(int n) {

        if (n >2 && n % 2 == 0) {
            return false;
        }
        
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    private static int numPrimotes(int inicio, int fim) {

        int nPrimotes = 0, nDigitos;
        boolean flag;

        for (int i = inicio; i <= fim; i++) {
            flag = true;
            nDigitos = (int) (Math.log10(i) + 1);
            int temp = i;
            for (int j = 0; j < nDigitos; j++) {
                
                if (!primo(temp)) {
                    flag = false;
                    break;
                }
                
                temp = rodarNumero(temp, nDigitos);
                
            }
            if(flag) {
                nPrimotes++;
            }

        }

        return nPrimotes;
    }

    private static int rodarNumero(int n, int numDig){
        
        int temp = (int)Math.pow(10, numDig - 1);
        int primDig = n / temp;
        
        n -= temp * primDig;
        n *= 10;
        n += primDig;
        
        return n;
    }
    
}
